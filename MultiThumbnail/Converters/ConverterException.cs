using System;

namespace MultiThumbnail.Converters
{
    [Serializable]
    public class ConverterException : Exception
    {
        public ConverterException()
        {
            
        }

        public ConverterException(string message):base(message)
        {
            
        }

        public ConverterException(string message, Exception exception):base(message, exception)
        {
            
        }

        public ConverterException(Exception baseException):base("Converter throws an exception", baseException)
        {
        }
    }
}