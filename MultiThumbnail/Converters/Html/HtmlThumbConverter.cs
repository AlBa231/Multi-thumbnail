﻿using System.IO;
using System.Text;

namespace MultiThumbnail.Converters.Html
{
    [Extension("HTM;HTML")]
    class HtmlThumbConverter : ThumbConverter
    {
        public override bool CreateMiniature(Stream fileStream, string resultPath)
        {
            var text = ReadText(fileStream);
            using (var htmlCapture = new HtmlCapture(ConverterSettings, resultPath))
            {

                htmlCapture.CreateHtml(text);

                htmlCapture.WaitTillFinished();

                return true;
            }
        }

        private static string ReadText(Stream fileStream)
        {
            using (var streamReader = new StreamReader(fileStream, Encoding.UTF8))
            {
                return streamReader.ReadToEnd();
            }
        }
    }
}
