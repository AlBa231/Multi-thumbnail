﻿using System;
using System.IO;
using LumiSoft.Net.Mime;

namespace MultiThumbnail.Converters.Html
{
    [Extension("MHT;EML")]
    class MimeThumbConverter : ThumbConverter
    {
        public override bool CreateMiniature(Stream fileStream, string resultPath)
        {
            using (var htmlCapture = new HtmlCapture(ConverterSettings, resultPath))
            {
                try
                {
                    Mime m = Mime.Parse(fileStream);
                    htmlCapture.CreateHtml(m.BodyHtml);
                }
                catch (FormatException){ return false; }
                htmlCapture.WaitTillFinished();
                //Thread.Sleep(5000);

                return true;
            }
        }

        //public void WaitTillFinished()
        //{

        //}

    }
}
