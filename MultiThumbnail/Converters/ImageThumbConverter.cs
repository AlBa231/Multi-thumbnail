﻿using System.Drawing;
using System.IO;

namespace MultiThumbnail.Converters
{
    [Extension("JPG;JPEG;PNG;BMP;TIFF;TIF;EMF;GIF;ICON;ICO;WMF")]
    class ImageThumbConverter:ThumbConverter
    {
        ///// <summary>
        ///// 
        ///// </summary>
        //public bool TrimScannedImage { get; set; }

        public override bool CreateMiniature(Stream fileStream, string resultPath)
        {
            using (var bitmap = new Bitmap(fileStream))
            {
                bitmap.Save(resultPath, ConverterSettings.ImageFormat);
            }
            return true;
        }
    }
}
