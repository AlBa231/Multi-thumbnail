﻿using System;
using System.IO;

namespace MultiThumbnail.Converters
{
    public abstract class ThumbConverter
    {
        public ConverterSettings ConverterSettings { get; set; }

        public virtual bool CreateMiniature(string path, string resultPath)
        {
            try
            {
                if (File.Exists(resultPath))
                    File.Delete(resultPath);
				var fileInfo = new FileInfo(path);
                if (!fileInfo.Exists || fileInfo.Length == 0)
                {
                    ThumbnailGenerator.Log("File not found - " + path);
                    return false;
                }
                return CreateMiniatureForFile(path, resultPath);
            }
            catch(FileNotFoundException)
            {
                throw;
            }
            catch(ArgumentException)
            {
                throw;
            }
            catch (Exception e)
            {
                ThumbnailGenerator.Log("Error - " + e.Message);
                if (ConverterSettings != null && ConverterSettings.ThrowExceptionOnError)
                    throw;
                throw new ConverterException(e);
            }
        }

    	protected virtual bool CreateMiniatureForFile(string path, string resultPath)
    	{
    		using (var fs = new FileStream(path, FileMode.Open, FileAccess.Read, FileShare.Read))
    		{
    			return CreateMiniature(fs, resultPath);
    		}
    	}

    	public abstract bool CreateMiniature(Stream fileStream, string resultPath);
    }
}
