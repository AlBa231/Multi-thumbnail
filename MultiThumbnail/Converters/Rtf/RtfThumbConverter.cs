﻿using System.Drawing;
using System.IO;
using System.Text;
using MultiThumbnail.Properties;

namespace MultiThumbnail.Converters.Rtf
{
    [Extension("RTF")]
    class RtfThumbConverter:ThumbConverter
    {
        public override bool CreateMiniature(Stream fileStream, string resultPath)
        {
            var text = ReadText(fileStream);
            using (var result = new Bitmap(ConverterSettings.ImageSize.Width, ConverterSettings.ImageSize.Height))
            {
                using (var graphics = Graphics.FromImage(result))
                {

                    //graphics.DrawString(text, new Font(FontFamily.GenericSansSerif, 14), Brushes.Black, new RectangleF(0, 0, ConverterSettings.ImageSize.Width, ConverterSettings.ImageSize.Height));
                    graphics.Clear(Color.White);
                    graphics.DrawRtfText(text, new Rectangle(0, 0, ConverterSettings.ImageSize.Width, ConverterSettings.ImageSize.Height));
                }
                result.Save(resultPath);
            }
            return true;
        }

        private static string ReadText(Stream fileStream)
        {
            using (var streamReader = new StreamReader(fileStream, Encoding.GetEncoding(Settings.Default.DefaultTextEncodingCodePage)))
            {
                return streamReader.ReadToEnd();
            }
        }
    }
}
