﻿using System;
using System.Collections.Generic;
using System.Globalization;

namespace MultiThumbnail
{
    [AttributeUsage(AttributeTargets.Class)]
    sealed class ExtensionAttribute:Attribute
    {
        private readonly List<string> _extensions = new List<string>();

        /// <summary>
        /// Setup an extension for converter. You can add several extensions.
        /// </summary>
        /// <param name="ext">The extension or several extensions splitted by semicolon. For Example: 'txt;pdf'</param>
        public ExtensionAttribute(string ext)
        {
            if (ext == null) throw new ArgumentNullException("ext");
            _extensions.AddRange(ext.ToUpper(CultureInfo.InvariantCulture).Split(new[]{';'}, StringSplitOptions.RemoveEmptyEntries));
        }

        public bool HasExtension(string ext)
        {
            return _extensions.Contains(ext);
        }

        public string[] Extensions
        {
            get { return _extensions.ToArray(); }
        }
    }
}
