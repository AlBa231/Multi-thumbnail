﻿using System.Drawing;
using System.Drawing.Imaging;

namespace MultiThumbnail
{
    public class ConverterSettings
    {
        /// <summary>
        /// Gets or sets the value that indicates whether need to throw or not the exception when converter has inner exception.
        /// </summary>
        public bool ThrowExceptionOnError { get; set; }

        private ImageFormat _imageFormat = ImageFormat.Jpeg;

        /// <summary>
        /// Gets or sets the type of result image. By default - jpeg.
        /// </summary>
        public ImageFormat ImageFormat
        {
            get { return _imageFormat; }
            set { _imageFormat = value; }
        }

        private Size _imageSize = new Size(1280, 1024);

        /// <summary>
        /// Gets or sets the default size of result thumbnails. Default 1024x680. 
        /// </summary>
        public Size ImageSize
        {
            get { return _imageSize; }
            set { _imageSize = value; }
        }

        /// <summary>
        /// The extension of current file
        /// </summary>
        public string CurrentExtension { get; set; }

        internal bool ConvertImageResult(string tmpPath, string resultPath)
        {
            using (var bitmap = new Bitmap(tmpPath))
            {
                ConvertImageResult(bitmap, resultPath);
            }
            return true;
        }

        internal void ConvertImageResult(Bitmap input, string resultPath)
        {
            using (var resizedImage = GraphicsManager.ResizeImage(input, ImageSize))
            {
                resizedImage.Save(resultPath, _imageFormat);
            }
        }
    }
}
