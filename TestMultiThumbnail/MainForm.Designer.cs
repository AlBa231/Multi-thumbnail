﻿namespace TestMultiThumbnail
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.pictureBox = new System.Windows.Forms.PictureBox();
            this.label7 = new System.Windows.Forms.Label();
            this.btnChooseScreenshotsDir = new System.Windows.Forms.Button();
            this.tbFilePathDir = new System.Windows.Forms.TextBox();
            this.openFileDialog = new System.Windows.Forms.OpenFileDialog();
            this.cbAllPages = new System.Windows.Forms.CheckBox();
            this.groupBox3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox)).BeginInit();
            this.SuspendLayout();
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.cbAllPages);
            this.groupBox3.Controls.Add(this.pictureBox);
            this.groupBox3.Controls.Add(this.label7);
            this.groupBox3.Controls.Add(this.btnChooseScreenshotsDir);
            this.groupBox3.Controls.Add(this.tbFilePathDir);
            this.groupBox3.Location = new System.Drawing.Point(12, 12);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(439, 427);
            this.groupBox3.TabIndex = 6;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Directories";
            // 
            // pictureBox
            // 
            this.pictureBox.Location = new System.Drawing.Point(9, 59);
            this.pictureBox.Name = "pictureBox";
            this.pictureBox.Size = new System.Drawing.Size(424, 362);
            this.pictureBox.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox.TabIndex = 5;
            this.pictureBox.TabStop = false;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.label7.Location = new System.Drawing.Point(6, 24);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(57, 13);
            this.label7.TabIndex = 4;
            this.label7.Text = "File Name:";
            // 
            // btnChooseScreenshotsDir
            // 
            this.btnChooseScreenshotsDir.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.btnChooseScreenshotsDir.Location = new System.Drawing.Point(321, 19);
            this.btnChooseScreenshotsDir.Name = "btnChooseScreenshotsDir";
            this.btnChooseScreenshotsDir.Size = new System.Drawing.Size(27, 22);
            this.btnChooseScreenshotsDir.TabIndex = 3;
            this.btnChooseScreenshotsDir.Text = "...";
            this.btnChooseScreenshotsDir.UseVisualStyleBackColor = true;
            this.btnChooseScreenshotsDir.Click += new System.EventHandler(this.btnChooseScreenshotsDir_Click);
            // 
            // tbFilePathDir
            // 
            this.tbFilePathDir.Location = new System.Drawing.Point(93, 21);
            this.tbFilePathDir.Name = "tbFilePathDir";
            this.tbFilePathDir.ReadOnly = true;
            this.tbFilePathDir.Size = new System.Drawing.Size(222, 20);
            this.tbFilePathDir.TabIndex = 2;
            // 
            // cbAllPages
            // 
            this.cbAllPages.AutoSize = true;
            this.cbAllPages.Location = new System.Drawing.Point(355, 24);
            this.cbAllPages.Name = "cbAllPages";
            this.cbAllPages.Size = new System.Drawing.Size(69, 17);
            this.cbAllPages.TabIndex = 6;
            this.cbAllPages.Text = "All pages";
            this.cbAllPages.UseVisualStyleBackColor = true;
            this.cbAllPages.CheckedChanged += new System.EventHandler(this.cbAllPages_CheckedChanged);
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(484, 451);
            this.Controls.Add(this.groupBox3);
            this.Name = "MainForm";
            this.Text = "MainForm";
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Button btnChooseScreenshotsDir;
        private System.Windows.Forms.TextBox tbFilePathDir;
        private System.Windows.Forms.OpenFileDialog openFileDialog;
        private System.Windows.Forms.PictureBox pictureBox;
        private System.Windows.Forms.CheckBox cbAllPages;
    }
}

