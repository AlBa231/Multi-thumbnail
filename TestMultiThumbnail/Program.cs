﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Threading;
using System.Windows.Forms;
using MultiThumbnail;

namespace TestMultiThumbnail
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main(params string[] args)
        {
			//args = new []{@"h:\My Documents\SVN\Wt Документы\!для Зала Царства\Графики\График встреч.docx", @"G:\Projects\Alba231\Alba231\Images\meets.jpg"};
            //Thread.Sleep(20000);
            ThumbnailGenerator.Log("Start Application. args - " + string.Join(", ", args));
            Application.ThreadException += Application_ThreadException;
			if (args.Length == 2)
			{
                ThumbnailGenerator.Log("Before convertion ");
			    try
			    {
    				DoConvertion(args[0], args[1]);
			    }
			    catch (Exception ex)
			    {
			        ThumbnailGenerator.Log("Exception - " + ex.Message);
			    }
                ThumbnailGenerator.Log("End convertion ");
				return;
			}
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new MainForm());
        }

        static void Application_ThreadException(object sender, ThreadExceptionEventArgs e)
        {
            ThumbnailGenerator.Log(String.Format(CultureInfo.InvariantCulture, "Exception {0}. \r\n Stack Trace: {1}", e.Exception.Message, e.Exception));
        }

		private static void DoConvertion(string fileFrom, string fileTo)
		{
			var generator = new ThumbnailGenerator();
			if (!generator.GenerateFor(fileFrom, fileTo))
                ThumbnailGenerator.Log("Generator for file not found. File - " + fileFrom);
		}
    }
}
